import RPLCD
import time


class screen:
    def __init__(self, data_pins, pin_rs, pin_e, cols=16, rows=2):
        self.lcd = CharLCD(cols=cols, rows=rows, pin_rs=pin_rs, pin_e=pin_e, pins_data=data_pins)


    def write(self, string):
        self.lcd.write_string(unicode(string, 'utf-8'))


    def clear(self):
        self.lcd.clear()

    
    def pos(self, row, col):
        self.lcd.cursor_pos = (row, col)
        
        
GPIO.setmode(GPIO.BCM)
lcd = screen([17,27,22,23],24,25)
lcd.write("Hello world")
time.sleep(3)
lcd.clear()
GPIO.cleanup()
