import cv2
import RPi.GPIO as GPIO
import time
from lcd import screen
from hg90s import HG90s
from hcsr4 import HCSR04
from detector import find_cars


# GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)


def main():
    #currently setting all the pins for 1s.
    #setting the RPI and pins:
    cam = cv2.VideoCapture(0)
    dis_sensor = HCSR04(1, 1)
    servo = HG90s(1)
    lcd = screen([1,2,3,4], 1, 1, cols=16, rows=2)

    #running the program in a loop.
    while True:
        lcd.write("Waiting For Car")
        #waiting for a car to get close to the gate
        while True:
            #messuring the distance to the closest object
            dis = dis_sensor.distance()
            #if an object is 10cm away from the gate
            if dis < 10:
                lcd.clear()
                break

        #checking if the object is a car.
        lcd.write("Checking If Eligible")
        _, frame = cam.read()
        car, frame = find_cars(frame)
        lcd.clear()

        #if a car is in the frame:
        if car:
            #writing msg on the lcd and opening the gate
            lcd.write("Opening Gate")
            servo.open()
            time.sleep(3)
            servo.close()
            lcd.clear()
        else:
            #not an eligible car. 
            lcd.write("Not Eligible")
            time.sleep(3)
            lcd.clear()


if __name__ == "__main__":
    main()



    

    
    
