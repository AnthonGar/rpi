# Libraries
import RPi.GPIO as GPIO
import time


class HCSR04:
    def __init__(self, trigger, echo):
        # setting the pins as members
        self._trigger_pin = trigger
        self._echo_pin = echo

        # settigns GPIO directions
        GPIO.setup(trigger, GPIO.OUT)
        GPIO.setup(echo, GPIO.IN)


    def distance(self):
        # set Trigger to HIGH
        GPIO.output(self._trigger_pin, True)

        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(self._trigger_pin, False)

        start_time = time.time()
        stop_time = time.time()

        # save StartTime
        while GPIO.input(self._echo_pin) == 0:
            start_time = time.time()

        # save time of arrival
        while GPIO.input(self._echo_pin) == 1:
            stop_time = time.time()

        # time difference between start and arrival
        travel_time = stop_time - start_time
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (travel_time * 34300) / 2
        return distance
