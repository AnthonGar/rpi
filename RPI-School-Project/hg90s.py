import RPi.GPIO as GPIO
import time


class HG90s:
    def __init__(self, servo):
        # setting pins as members of the class
        self._servo_pin = servo

        # setting GPIO pins
        GPIO.setup(servo, GPIO.OUT)

        # resetting the servo and saving the position as a member.
        self._pulse = GPIO.PWM(servo, 50)
        self._pulse.start(2.5)


    def open(self):
        # turning the servo 90 deg.
        self._pulse.ChangeDutyCycle(7.5)


    def close(self):
        # turning the servo 90 deg to the other direction.
        self._pulse.ChangeDutyCycle(2.5)
