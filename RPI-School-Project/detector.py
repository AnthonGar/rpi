import cv2


def find_car(frame):
    #load the car cascade
    car_cascade_module = cv2.CascadeClassifier('cars.xml')
    #convert BRG image to GrayScale image
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #Comparing objects in the image to a cascade of a car.
    cars = car_cascade_module.detectMultiScale(gray_frame, 1.1, 1)
    #if a there is a car in the frame
    print(cars)
    if type(cars) == type(()):
        #expecting one car in the frame.
        x, y, w, h = cars[0]
        #adding a red rectange to mark the car on the frame.
        cv2.rectangle(frame, (x,y), (x+w, y+h), (100,100,100), 2)
        #sending a tupple with a boolean that indicates that a car is found in the frame + updated frame.
        return (True, frame)
    else:
        #sending a tupple with a boolean that indicates that a car isn't found in the frame.
        return (False, frame)


def find_cars(frame):
    #load the car cascade
    car_cascade_module = cv2.CascadeClassifier('cars.xml')
    #convert BRG image to GrayScale image
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #Comparing objects in the image to a cascade of a car.
    cars = car_cascade_module.detectMultiScale(gray_frame, 1.078, 1)
    #if a there is a car in the frame
    print(cars)
    if type(cars) == type(()):
        for (x, y, w, h) in cars:
            #adding a red rectange to mark the car on the frame.
            cv2.rectangle(frame, (x,y), (x+w, y+h), (100,100,100), 2)
        #sending a tupple with a boolean that indicates that a car is found in the frame + updated frame.
        return (True, frame)
    else:
        #sending a tupple with a boolean that indicates that a car isn't found in the frame.
        return (False, frame)
        
frame = cv2.imread('highway.jpeg')
flag, frame = find_cars(frame)
cv2.imshow('Preview', frame)
while True:
    if cv2.waitKey(33) == 27:
        break
cv2.destroyAllWindows()